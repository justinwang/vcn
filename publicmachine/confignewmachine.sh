#! /bin/bash

declare Username=vcn

# input the ipaddress of the new machine or you can input as a argument of the command like bash newmachine 192.168.1.1
if [ $1 != '' ] 
   then
     IPaddress=$1
   else
   echo "please input the IP adress of the machine you want to config"
   read IPaddress
    
fi 
#check the validation of the ip address
if [[ $IPaddress =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        OIFS=$IFS
        IFS='.'
        ip=($IPaddress)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi

if [ $stat == 0 ]
  then
   echo "[newmachine]
$IPaddress ansible_connection=ssh ansible_ssh_user=root ansible_ssh_pass=volunteer" > ./hosts

    sshpass -p 'volunteer' ssh -t vcn@$IPaddress "sudo mkdir /home/vcn/.ssh/"

	sshpass -p 'volunteer' scp -o "StrictHostKeyChecking no" /home/vcn/.ssh/id_rsa.pub vcn@$IPaddress:/home/vcn/.ssh/authorized_keys


	sshpass -p 'volunteer' ssh -t vcn@$IPaddress "sudo mkdir /root/.ssh/ & sudo cp -f /home/vcn/.ssh/authorized_keys /root/.ssh/"

  ansible-playbook -i hosts newmachine.yml
  else
   echo " invalid ip address you input, the programm has been broken, run it again please" 
fi

