This is for the configuration of a new public machine with Ubuntu, the following will be configured:

1. install MFC9970 printer.
2. setup guess session(for public computer). if need it, please enter the public folder and run the following command.
3. change the home page of firefox(for public computer). if need, enter homepage folder and execute the changehomepage.sh with a ip address. 



Before run the program, you need to install ssh on the remote machine, and get the ip address of the remote machine. 
On your local machine, install  ansible and sshpass on the local machin.
and generate a ssh pub key by running the command as follows if there are not run yet:
sudo apt-get install ansible -y
sudo apt-get install sshpass -y
sudo apt-get install ssh -y
ssh-keygen


Download all the files and extract

Then, find the ip address of the newmachine which you want to install printer by the command on that machine: 
ip address


On your machine, Enter the directory you extracted, open a terminal on this directory, and run the command:
bash confignewmachine.sh [IP address]
i.e. bash confignewmachine.sh 192.168.20.151

